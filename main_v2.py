import concurrent
import json
import os
import concurrent.futures
import threading

from utils.svn_utils import get_log, process_log


def main():
    global list_ignore
    global list_deliveriables
    with open('list_ignore.json', 'r', encoding='utf-8') as file:
        list_ignore = json.load(file)

    with open('list_deliveriables.json', 'r', encoding='utf-8') as file:
        list_deliveriables = json.load(file)

    print("-----------------Starting to collect comment-----------------")

    logs = get_log(svn_url)
    # logs = [item for item in logs if 7220 <= int(item['version'][1:]) <= 7291]

    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_THREADS) as executor:
        futures = [executor.submit(
            process_log,
            log,
            logs,
            list_deliveriables,
            list_ignore,
            output_file,
            excel_lock,
            current_directory,
            svn_url
        ) for log in logs]

        for future in concurrent.futures.as_completed(futures):
            try:
                future.result()
            except Exception as e:
                print(f"An error occurred: {e}")

    print(f"-----------------Finished to collect comment-----------------")


if __name__ == "__main__":
    MAX_THREADS = 8
    excel_lock = threading.Lock()
    current_directory = os.getcwd() + '\\tmp'
    svn_url = r"https://rikai.backlog.com/svn/BIPROGY_PRIMO_ALLINONE/03.WIP/05.PhysicalDesign/02.FinishedDesign"
    output_file = r'data\output_comments_3.xlsx'
    list_auth = []
    list_deliveriables = []
    list_ignore = []

    main()
