# Auto collect comment - INSTALLATION GUIDE

## Environment:
    - vscode
    - python 3.9
    - venv
    - snv + svn-cli

# Setup
  - `python -m venv <name_venv>`
  - `<name_venv>\Scripts\activate`
  - `pip install -r requirements.txt`

# Run
  - `python main.py or python main_v2.py`

# Structure
  - `data`: Nơi lưu output.
  - `tmp`: Nơi lưu cache.
  - `list_ignore.json`: Loại bỏ những comment theo một format {"sheet": SheetName, "cell": ColumnRow}.
  - `list_deliveriables.json`: Lấy từ tài liệu bên dưới để mapping.
  - `utils`: Nơi chứa chức năng và logic xử lý

# Documents
  - https://docs.google.com/spreadsheets/d/1X66dEDWo1ePIEQcF1AIEhTYDGPd3n_A2/edit#gid=484447042
  - https://docs.google.com/spreadsheets/d/1X66dEDWo1ePIEQcF1AIEhTYDGPd3n_A2/edit#gid=484447042
  - https://rikai.backlog.com/alias/wiki/498295

# Note
  - Phải đăng nhập vào SVN trước khi chạy
  - Khi chạy thì không mở file output trong thư mục data để tránh xảy ra lỗi.
  - Nếu lỗi hãy xóa cache trong thư mục tmp.
  - Chạy đa luồng nên thứ tự sẽ lộn xộn, có thể chỉnh lại thành 1 luồng ở phần MAX_THREADS.
  - Run ngày 29/03/2024 có khoảng 1313 commit, thời gian thực hiện xong khi chạy với 8 thread khoảng 4-5h.
  - Có thể điều chỉnh khoảng commit muốn thực thi ở file main.py hoặc main_v2.py
  - Lấy theo một khoảng giữa các version commit -> chỉnh sửa code theo cấu trúc `[item for item in logs if min_version <= int(item['version'][1:]) <= max_version]`