import re
import openpyxl

from utils.excel_utils import save_review_info, save_comment_info


def collect_comments(file_path, more_info, list_deliveriables, list_ignore, logs, output_file, excel_lock):
    date_time = re.findall(r'\d{4}-\d{2}-\d{2}', more_info['date'])[0].replace("-", "")
    fid = re.search(r'[FR](\d+)', more_info['file_name'])[0]
    filter_author_commit = [item for item in logs if item['author'] == more_info['author'] and item['date'] == more_info['date']][0]
    list_author_comment = []
    artifact_id = f'{fid}_1'
    no_of_review = filter_author_commit['no_of_review']

    for item in list_deliveriables:
        if item['name'] in more_info['file_name']:
            artifact_id = item['id']

    save_review_info({
        "artifact_id": artifact_id,
        "review_id": f'{artifact_id}_BIP_{date_time}_{f"0{no_of_review}" if no_of_review < 10 else no_of_review}',
        'date_time': more_info['date'],
        'author_commit': more_info['author'],
    }, output_file, excel_lock)

    workbook = openpyxl.load_workbook(file_path)
    for sheet_name in workbook.sheetnames:
        sheet = workbook[sheet_name]
        for row in sheet.iter_rows():
            for cell in row:
                object_to_check = {'sheet': sheet_name, 'cell': cell.coordinate}
                is_in_list_ignore = False

                for item in list_ignore:
                    if all(item[key] == object_to_check[key] for key in object_to_check.keys()):
                        is_in_list_ignore = True
                        break

                if cell.comment and not is_in_list_ignore:
                    no_of_comment = 1

                    if not any(o['author_comment'] == cell.comment.author for o in list_author_comment):
                        list_author_comment.append({
                            'author_comment': cell.comment.author,
                            'no_of_comment': 1
                        })

                    for o in list_author_comment:
                        if o['author_comment'] == cell.comment.author:
                            no_of_comment = o['no_of_comment']

                    comment = {
                        'sheet': sheet_name,
                        'cell': cell.coordinate,
                        'comment': cell.comment.text.replace("[Threaded comment]\n\nYour version of Excel allows you "
                                                             "to read this threaded comment; however, any edits to it "
                                                             "will get removed if the file is opened in a newer "
                                                             "version of Excel. Learn more: "
                                                             "https://go.microsoft.com/fwlink/?linkid=870924\n"
                                                             "\nComment:\n", ""),
                        'author_comment': cell.comment.author,
                        'author_commit': more_info['author'],
                        'date_time': more_info['date'],
                        'version': more_info['version'],
                        'file_name': more_info['file_name'],
                        'artifact_id': artifact_id,
                        'review_id': f'{artifact_id}_BIP_{date_time}_{f"0{no_of_review}" if no_of_review < 10 else no_of_review}',
                        'comment_id': f'{artifact_id}_BIP_{date_time}_{f"0{no_of_review}" if no_of_review < 10 else no_of_review}_{f"0{no_of_comment}" if no_of_comment < 10 else no_of_comment}',
                    }

                    for o in list_author_comment:
                        if o['author_comment'] == cell.comment.author:
                            o['no_of_comment'] += 1

                    save_comment_info(comment, output_file, excel_lock)

    filter_author_commit['no_of_review'] += 1

