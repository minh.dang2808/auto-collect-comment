import os


def browse_directory(directory_path):
    results = []
    for file_name in os.listdir(directory_path):
        file_path = os.path.join(directory_path, file_name)
        if os.path.isfile(file_path) and ".xlsx" in file_name:
            results.append(file_name)

    return results
