import os

import openpyxl


def save_review_info(data, output_file, excel_lock):
    try:
        with excel_lock:
            if os.path.exists(output_file):
                workbook = openpyxl.load_workbook(output_file)
                if 'PD Review Info' in workbook.sheetnames:
                    sheet = workbook['PD Review Info']
                else:
                    sheet = workbook.create_sheet('PD Review Info')
                    headers = ['レビューID', '成果物ID', 'レビュ種別', '参加人数', '時間', '予定日', '実施日', '被レビュア', '被レビュア', 'レビュア', '対象外フラグ', '備考']
                    for col_idx, header in enumerate(headers, start=1):
                        sheet.cell(row=1, column=col_idx, value=header)
            else:
                workbook = openpyxl.Workbook()
                sheet = workbook.active
                sheet.title = 'PD Review Info'
                headers = ['レビューID', '成果物ID', 'レビュ種別', '参加人数', '時間', '予定日', '実施日', '被レビュア', 'レビュア', '対象外フラグ', '備考']
                for col_idx, header in enumerate(headers, start=1):
                    sheet.cell(row=1, column=col_idx, value=header)

            next_row = sheet.max_row + 1

            sheet.cell(row=next_row, column=1, value=data['review_id'])
            sheet.cell(row=next_row, column=2, value=data['artifact_id'])
            sheet.cell(row=next_row, column=3, value="RIKAI VNレビュ")
            sheet.cell(row=next_row, column=4, value="1")
            sheet.cell(row=next_row, column=5, value="1")
            sheet.cell(row=next_row, column=6, value=data['date_time'])
            sheet.cell(row=next_row, column=7, value=data['date_time'])
            sheet.cell(row=next_row, column=8, value="")
            sheet.cell(row=next_row, column=9, value=data['author_commit'])
            sheet.cell(row=next_row, column=10, value="")
            sheet.cell(row=next_row, column=11, value="")

            workbook.save(output_file)

            print(f"Saved PD Review Info")

    except Exception as e:
        print(f"save excel: {e}")


def save_comment_info(comment, output_file, excel_lock):
    try:
        with excel_lock:
            if os.path.exists(output_file):
                workbook = openpyxl.load_workbook(output_file)
                if 'PD Comment Info' in workbook.sheetnames:
                    sheet = workbook['PD Comment Info']

                    for row in sheet.iter_rows(min_row=2):
                        if f"Sheet: {comment['sheet']}\nDòng: {comment['cell']}\nNội dung: {comment['comment']}" == \
                                row[2].value and comment['author_comment'] == row[3].value:
                            return
                else:
                    sheet = workbook.create_sheet('PD Comment Info')
                    headers = ['不具合ID', 'レビューID', '不具合内容', 'Author']
                    for col_idx, header in enumerate(headers, start=1):
                        sheet.cell(row=1, column=col_idx, value=header)
            else:
                workbook = openpyxl.Workbook()
                sheet = workbook.active
                sheet.title = 'PD Comment Info'
                headers = ['不具合ID', 'レビューID', '不具合内容', 'Author']
                for col_idx, header in enumerate(headers, start=1):
                    sheet.cell(row=1, column=col_idx, value=header)

            next_row = sheet.max_row + 1

            sheet.cell(row=next_row, column=1, value=comment['comment_id'])
            sheet.cell(row=next_row, column=2, value=comment['review_id'])
            sheet.cell(row=next_row, column=3,
                       value=f"Sheet: {comment['sheet']}\nDòng: {comment['cell']}\nNội dung: {comment['comment']}")
            sheet.cell(row=next_row, column=4, value=comment['author_comment'])

            workbook.save(output_file)

            print(f"Saved PD Comment Info: file_name={comment['file_name']}, sheet={comment['sheet']}, cell={comment['cell']}")

    except Exception as e:
        print(f"save excel: {e}")
