import os
import subprocess
import re

from utils.comment_collector import collect_comments
from utils.file_utils import browse_directory


def get_log(svn_url):
    logs_text = subprocess.run(["svn", "log", svn_url], capture_output=True, text=True)
    pattern = r'r\d+ \| [^\|]+ \| .* \d+ line'
    logs_array = [parse_log_entry(entry) for entry in re.findall(pattern, logs_text.stdout)][::-1]
    return logs_array


def parse_log_entry(entry):
    parts = entry.split('|')
    version = parts[0].strip()
    author = parts[1].strip()
    date = parts[2].strip()
    return {'version': version, 'author': author, 'date': date, 'no_of_review': 1}


def checkout_version(current_directory, svn_url, version):
    new_path_folder = f'{current_directory}\\{version}'
    if not os.path.exists(new_path_folder):
        os.makedirs(new_path_folder)
        subprocess.run(["svn", "checkout", "-r", version, svn_url, new_path_folder], capture_output=True, text=True)

    return new_path_folder


def check_change_in_version(version, path):
    logs_text = subprocess.run(["svn", "log", "-r", version, path, "-v"], capture_output=True, text=True)
    pattern = r'02\.FinishedDesign.*\.xlsx'
    logs_array = re.findall(pattern, logs_text.stdout)[::-1]
    return logs_array


def process_log(log, logs, list_deliveriables, list_ignore, output_file, excel_lock, current_directory, svn_url):
    try:
        version_path_folder = checkout_version(current_directory, svn_url, log['version'])
        list_change = check_change_in_version(log['version'], version_path_folder)
        file_names = browse_directory(version_path_folder)

        for file_name in file_names:
            is_found = False
            fid = re.search(r'[FR](\d+)', file_name)[0] if re.search(r'[FR](\d+)', file_name) else None

            for item in list_change:
                if fid and fid in item:
                    is_found = True
                    break

            if is_found:
                log['file_name'] = file_name
                collect_comments(
                    file_path=version_path_folder + f'\\{file_name}',
                    more_info=log,
                    list_deliveriables=list_deliveriables,
                    list_ignore=list_ignore,
                    logs=logs,
                    output_file=output_file,
                    excel_lock=excel_lock
                )
    except Exception as e:
        print(f"Error in version: {log['version']}, {e}")