import json
import os
import re
import threading

import openpyxl
import subprocess
import concurrent.futures


def collect_comments(file_path, more_info):
    date_time = re.findall(r'\d{4}-\d{2}-\d{2}', more_info['date'])[0].replace("-", "")
    fid = re.search(r'[FR](\d+)', more_info['file_name'])[0]
    filter_author_commit = [item for item in logs if item['author'] == more_info['author'] and item['date'] == more_info['date']][0]
    list_author_comment = []
    artifact_id = f'{fid}_1'
    no_of_review = filter_author_commit['no_of_review']

    for item in list_deliveriables:
        if item['name'] in more_info['file_name']:
            artifact_id = item['id']

    save_review_info({
        "artifact_id": artifact_id,
        "review_id": f'{artifact_id}_BIP_{date_time}_{f"0{no_of_review}" if no_of_review < 10 else no_of_review}',
        'date_time': more_info['date'],
        'author_commit': more_info['author'],
    }, output_file)

    workbook = openpyxl.load_workbook(file_path)
    for sheet_name in workbook.sheetnames:
        sheet = workbook[sheet_name]
        for row in sheet.iter_rows():
            for cell in row:
                object_to_check = {'sheet': sheet_name, 'cell': cell.coordinate}
                is_in_list_ignore = False

                for item in list_ignore:
                    if all(item[key] == object_to_check[key] for key in object_to_check.keys()):
                        is_in_list_ignore = True
                        break

                if cell.comment and not is_in_list_ignore:
                    no_of_comment = 1

                    if not any(o['author_comment'] == cell.comment.author for o in list_author_comment):
                        list_author_comment.append({
                            'author_comment': cell.comment.author,
                            'no_of_comment': 1
                        })

                    for o in list_author_comment:
                        if o['author_comment'] == cell.comment.author:
                            no_of_comment = o['no_of_comment']

                    comment = {
                        'sheet': sheet_name,
                        'cell': cell.coordinate,
                        'comment': cell.comment.text.replace("[Threaded comment]\n\nYour version of Excel allows you "
                                                             "to read this threaded comment; however, any edits to it "
                                                             "will get removed if the file is opened in a newer "
                                                             "version of Excel. Learn more: "
                                                             "https://go.microsoft.com/fwlink/?linkid=870924\n"
                                                             "\nComment:\n", ""),
                        'author_comment': cell.comment.author,
                        'author_commit': more_info['author'],
                        'date_time': more_info['date'],
                        'version': more_info['version'],
                        'file_name': more_info['file_name'],
                        'artifact_id': artifact_id,
                        'review_id': f'{artifact_id}_BIP_{date_time}_{f"0{no_of_review}" if no_of_review < 10 else no_of_review}',
                        'comment_id': f'{artifact_id}_BIP_{date_time}_{f"0{no_of_review}" if no_of_review < 10 else no_of_review}_{f"0{no_of_comment}" if no_of_comment < 10 else no_of_comment}',
                    }

                    for o in list_author_comment:
                        if o['author_comment'] == cell.comment.author:
                            o['no_of_comment'] += 1

                    # save_comment_to_excel(comment, r'data\output_comments_2.xlsx')
                    save_comment_info(comment, output_file)

    filter_author_commit['no_of_review'] += 1


def comment_exists(comment, output_file):
    if not os.path.exists(output_file):
        return False

    workbook = openpyxl.load_workbook(output_file)
    sheet = workbook.active

    for row in sheet.iter_rows(min_row=2):
        if f"Sheet: {comment['sheet']}\nDòng: {comment['cell']}\nNội dung: {comment['comment']}" == row[8].value \
                and comment['author_comment'] == row[5].value:
            return True

    return False


def save_review_info(data, output_file):
    try:
        with excel_lock:
            if os.path.exists(output_file):
                workbook = openpyxl.load_workbook(output_file)
                if 'PD Review Info' in workbook.sheetnames:
                    sheet = workbook['PD Review Info']
                else:
                    sheet = workbook.create_sheet('PD Review Info')
                    headers = ['レビューID', '成果物ID', 'レビュ種別', '参加人数', '時間', '予定日', '実施日', '被レビュア', '被レビュア', 'レビュア', '対象外フラグ', '備考']
                    for col_idx, header in enumerate(headers, start=1):
                        sheet.cell(row=1, column=col_idx, value=header)
            else:
                workbook = openpyxl.Workbook()
                sheet = workbook.active
                sheet.title = 'PD Review Info'
                headers = ['レビューID', '成果物ID', 'レビュ種別', '参加人数', '時間', '予定日', '実施日', '被レビュア', 'レビュア', '対象外フラグ', '備考']
                for col_idx, header in enumerate(headers, start=1):
                    sheet.cell(row=1, column=col_idx, value=header)

            next_row = sheet.max_row + 1

            sheet.cell(row=next_row, column=1, value=data['review_id'])
            sheet.cell(row=next_row, column=2, value=data['artifact_id'])
            sheet.cell(row=next_row, column=3, value="RIKAI VNレビュ")
            sheet.cell(row=next_row, column=4, value="1")
            sheet.cell(row=next_row, column=5, value="1")
            sheet.cell(row=next_row, column=6, value=data['date_time'])
            sheet.cell(row=next_row, column=7, value=data['date_time'])
            sheet.cell(row=next_row, column=8, value="")
            sheet.cell(row=next_row, column=9, value=data['author_commit'])
            sheet.cell(row=next_row, column=10, value="")
            sheet.cell(row=next_row, column=11, value="")

            workbook.save(output_file)

            print(f"Saved PD Review Info")

    except Exception as e:
        print(f"save excel: {e}")


def save_comment_info(comment, output_file):
    try:
        with excel_lock:
            if os.path.exists(output_file):
                workbook = openpyxl.load_workbook(output_file)
                if 'PD Comment Info' in workbook.sheetnames:
                    sheet = workbook['PD Comment Info']

                    for row in sheet.iter_rows(min_row=2):
                        if f"Sheet: {comment['sheet']}\nDòng: {comment['cell']}\nNội dung: {comment['comment']}" == \
                                row[2].value and comment['author_comment'] == row[3].value:
                            return
                else:
                    sheet = workbook.create_sheet('PD Comment Info')
                    headers = ['不具合ID', 'レビューID', '不具合内容', 'Author']
                    for col_idx, header in enumerate(headers, start=1):
                        sheet.cell(row=1, column=col_idx, value=header)
            else:
                workbook = openpyxl.Workbook()
                sheet = workbook.active
                sheet.title = 'PD Comment Info'
                headers = ['不具合ID', 'レビューID', '不具合内容', 'Author']
                for col_idx, header in enumerate(headers, start=1):
                    sheet.cell(row=1, column=col_idx, value=header)

            next_row = sheet.max_row + 1

            sheet.cell(row=next_row, column=1, value=comment['comment_id'])
            sheet.cell(row=next_row, column=2, value=comment['review_id'])
            sheet.cell(row=next_row, column=3,
                       value=f"Sheet: {comment['sheet']}\nDòng: {comment['cell']}\nNội dung: {comment['comment']}")
            sheet.cell(row=next_row, column=4, value=comment['author_comment'])

            workbook.save(output_file)

            print(f"Saved PD Comment Info: file_name={comment['file_name']}, sheet={comment['sheet']}, cell={comment['cell']}")

    except Exception as e:
        print(f"save excel: {e}")


def browse_directory(directory_path):
    results = []
    for file_name in os.listdir(directory_path):
        file_path = os.path.join(directory_path, file_name)
        if os.path.isfile(file_path) and ".xlsx" in file_name:
            results.append(file_name)

    return results


def get_log():
    logs_text = subprocess.run(["svn", "log", svn_url], capture_output=True, text=True)
    pattern = r'r\d+ \| [^\|]+ \| .* \d+ line'
    logs_array = [parse_log_entry(entry) for entry in re.findall(pattern, logs_text.stdout)][::-1]
    return logs_array


def parse_log_entry(entry):
    parts = entry.split('|')
    version = parts[0].strip()
    author = parts[1].strip()
    date = parts[2].strip()
    return {'version': version, 'author': author, 'date': date, 'no_of_review': 1}


def checkout_version(version):
    new_path_folder = f'{current_directory}\\{version}'
    if not os.path.exists(new_path_folder):
        os.makedirs(new_path_folder)
        subprocess.run(["svn", "checkout", "-r", version, svn_url, new_path_folder], capture_output=True, text=True)

    return new_path_folder


def check_change_in_version(version, path):
    logs_text = subprocess.run(["svn", "log", "-r", version, path, "-v"], capture_output=True, text=True)
    pattern = r'02\.FinishedDesign.*\.xlsx'
    logs_array = re.findall(pattern, logs_text.stdout)[::-1]
    return logs_array


def process_log(log):
    try:
        version_path_folder = checkout_version(log['version'])
        list_change = check_change_in_version(log['version'], version_path_folder)
        file_names = browse_directory(version_path_folder)

        for file_name in file_names:
            is_found = False
            fid = re.search(r'[FR](\d+)', file_name)[0] if re.search(r'[FR](\d+)', file_name) else None

            for item in list_change:
                if fid and fid in item:
                    is_found = True
                    break

            if is_found:
                log['file_name'] = file_name
                collect_comments(version_path_folder + f'\\{file_name}', more_info=log)
    except Exception as e:
        print(f"Error in version: {log['version']}, {e}")


# MAX_THREADS = os.cpu_count() or 1
MAX_THREADS = 8
excel_lock = threading.Lock()
current_directory = os.getcwd() + '\\tmp'
svn_url = r"https://rikai.backlog.com/svn/BIPROGY_PRIMO_ALLINONE/03.WIP/05.PhysicalDesign/02.FinishedDesign"
output_file = r'data\output_comments1.xlsx'
list_auth = []

with open('list_ignore.json', 'r', encoding='utf-8') as file:
    list_ignore = json.load(file)

with open('list_deliveriables.json', 'r', encoding='utf-8') as file:
    list_deliveriables = json.load(file)

print("-----------------Starting to collect comment-----------------")

logs = get_log()
# logs = [item for item in logs if 7220 <= int(item['version'][1:]) <= 7291]
# logs = [item for item in logs if 10694 <= int(item['version'][1:])]
with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_THREADS) as executor:
    futures = [executor.submit(process_log, log) for log in logs]

    for future in concurrent.futures.as_completed(futures):
        try:
            future.result()
        except Exception as e:
            print(f"An error occurred: {e}")

print(f"-----------------Finished to collect comment-----------------")
